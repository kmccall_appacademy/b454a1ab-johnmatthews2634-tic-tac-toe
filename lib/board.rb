class Board
  attr_reader :grid

  def initialize(number = 3)
    if number == 3
      @grid = Array.new(number) { Array.new(number) }
    else
      @grid = number
    end
  end

  def [](position)
    row, col = position
    @grid[row][col]
  end

  def []=(position, mark)
    row, col = position
    @grid[row][col] = mark
  end

  def place_mark(position, mark)
    row, col = position
    if empty?(position)
      @grid[row][col] = mark
    else
      raise "Not an empty position"
    end
  end

  def empty?(position)
    row, col = position
    @grid[row][col] == nil

  end

  def winner
    #Determining if someone won by row
    @grid.each do |subarray|
      if subarray.all? {|mark| mark == :X} || subarray.all? {|mark| mark == :O}
        return subarray[0]
      end
    end

    #Determining if someone won by column
    number_of_rows = @grid.length
    array_idx = 0
    column_idx = 0
    columns_array = []

    while column_idx < number_of_rows
      new_subarray = []
      array_idx = 0
      while array_idx < number_of_rows
        new_subarray << @grid[array_idx][column_idx]
        array_idx += 1
      end
      columns_array << new_subarray
      column_idx += 1
    end

    columns_array.each do |subarray|
      if subarray.all? {|mark| mark == :X} || subarray.all? {|mark| mark == :O}
        return subarray[0]
      end
    end

    #Determining if someone won by diagonal

    column_idx = 0
    diagonals_array = []

    while column_idx < number_of_rows
      diagonals_array << @grid[column_idx][column_idx]
      column_idx += 1
    end

    if diagonals_array.all? {|mark| mark == :X} || diagonals_array.all? {|mark| mark == :O}
      return diagonals_array[0]
    end

    array_idx = 0
    column_idx = number_of_rows - 1
    diagonals_array = []

    while array_idx < number_of_rows
      diagonals_array << @grid[array_idx][column_idx]
      column_idx -= 1
      array_idx += 1
    end

    if diagonals_array.all? {|mark| mark == :X} || diagonals_array.all? {|mark| mark == :O}
      return diagonals_array[0]
    end

    return nil

  end

  def over?
    if @grid.none? {|arr| arr.include?(nil)} || winner
      return true
    else
      return false
    end
  end


end

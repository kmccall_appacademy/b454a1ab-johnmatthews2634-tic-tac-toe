require_relative 'board'
require_relative 'human_player'
require_relative 'computer_player'

class Game
  attr_accessor :player1, :player2, :board, :current_player

  def initialize(player1, player2)
    @player1 = player1
    @player2 = player2
    @player1.mark = :O
    @player2.mark = :X
    @board = Board.new
    @current_player = player1
  end

  def switch_players!
    if @current_player == player1
      @current_player = player2
    else
      @current_player = player1
    end
  end

  def play_turn
    board.place_mark(current_player.get_move, current_player.mark)
    switch_players!
  end

  def play
    until board.over?
      play_turn
    end

  end
end

class ComputerPlayer
  attr_reader :name, :board
  attr_accessor :mark
  def initialize(name)
    @mark = :O
    @name = name
  end

  def display(board)
    @board = board
  end


  def get_move

    potential_moves = []
    (0..2).each do |row|
      (0..2).each do |col|
        position = [row, col]
        if board[position].nil?
          potential_moves << position
        end
      end
    end

    potential_moves.each do |move|
      row = move[0]
      col = move[1]
      position = [row, col]
      board[position] = mark
      if board.winner == mark
        return move
      else
        board[move] = nil
      end
    end
    potential_moves.shuffle[0]

  end
end
